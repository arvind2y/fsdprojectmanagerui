import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './features/project/project.component';
import { UserComponent } from './features/user/user.component';
import { TaskComponent } from './features/task/task.component';
import { ViewTaskComponent } from './features/view-task/view-task.component';

const routes: Routes = [
  { path: 'project', component: ProjectComponent },
  { path: 'user', component: UserComponent },
  { path: 'task', component: TaskComponent },
  { path: 'viewtask', component: ViewTaskComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
