import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Serviceerror } from '../models/serviceerror';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BaseService {

    constructor() { }

    public baseurl(): string {
        console.log(environment.apiBaseUrl);
        return environment.apiBaseUrl;
    }

    //Fetch the data from the respective service.
    protected getData(res: Response) {

        const responseBody = res.json();
        let resStatus = responseBody.Status;
        console.log(resStatus);
        console.log(responseBody);

        switch (resStatus) {
            case 'success':
                return responseBody.Data;
            case 'fail':
                throw new Serviceerror(responseBody.Message, responseBody.data, 'fail');
            case 'error':
                throw new Serviceerror(responseBody.Message, responseBody.data);
            default:
                throw new Serviceerror(`Invalid JSON Response Status [ ${responseBody.Status}]`);

        }


        // if (body.Status === 'success') {
        //     return body.Data;
        // } else if (body.Status === 'fail') {
        //     throw new Serviceerror(body.Message, body.Data, 'fail');
        // } else if (body.Status === 'error') {
        //     throw new Serviceerror(body.Message, body.Data);
        // } else {
        //     throw new Serviceerror(`Invalid JSON. Response Status [ ${body.Status}]`);
        // }
    }


    //Generic handeler method
    protected handleError(error: any) {

        let errMsg = "";
        if (error instanceof Serviceerror) {
            return throwError(error);
        } else {

            if (error.status === 0) { errMsg = "Service is not running, Please make sure server is up and running!"; }
            else {
                errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
            }

            return throwError(new Serviceerror(errMsg));
        }
    }
}
